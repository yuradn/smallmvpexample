package yurasik.com.smallmvpexample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.concurrent.ExecutionException;

import yurasik.com.smallmvpexample.mvp.presenter.CreatePersonPresenter;
import yurasik.com.smallmvpexample.mvp.view.CreatePersonMvpView;

public class MainActivity extends AppCompatActivity implements CreatePersonMvpView,
        View.OnClickListener {
    private CreatePersonPresenter<CreatePersonMvpView> mPresenter;

    protected EditText edtFirstName;
    protected EditText edtLastName;
    protected Button btnSave;
    protected Button btnClear;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edtFirstName = EditText.class.cast(findViewById(R.id.edt_first_name));
        edtLastName = EditText.class.cast(findViewById(R.id.edt_last_name));
        btnSave = Button.class.cast(findViewById(R.id.btn_save));
        btnClear = Button.class.cast(findViewById(R.id.btn_clear));

        btnSave.setOnClickListener(this);
        btnClear.setOnClickListener(this);

        mPresenter = new CreatePersonPresenter<>();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mPresenter.onSave(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mPresenter.onRestote(savedInstanceState);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mPresenter.attach(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mPresenter.detach();
    }

    @Override
    public String toString() {
        return "MainActivity{}";
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_save:
                mPresenter.onClickSaveButton();
                break;
            case R.id.btn_clear:
                mPresenter.onClickClearButton();
                break;
            default:
                try {
                    throw new Exception("No id found!");
                } catch (Exception e) {
                    e.printStackTrace();
                }
        }
    }

    @Override
    public String getFirstName() {
        return edtFirstName.getText().toString();
    }

    @Override
    public String getLastName() {
        return edtLastName.getText().toString();
    }

    @Override
    public void clearEditTexts() {
        edtFirstName.setText("");
        edtLastName.setText("");
    }
}
