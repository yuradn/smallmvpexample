package yurasik.com.smallmvpexample.data;

import yurasik.com.smallmvpexample.mvp.model.Person;

/**
 * Created by Yurii on 6/15/17.
 */

public interface DataStorageInterface {
    void savePerson(Person person);
}
