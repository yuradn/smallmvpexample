package yurasik.com.smallmvpexample.data;

import yurasik.com.smallmvpexample.mvp.model.Person;
import yurasik.com.smallmvpexample.util.LogUtil;

/**
 * Created by Yurii on 6/15/17.
 */

public class MockDataStorage implements DataStorageInterface {
    @Override
    public void savePerson(Person person) {
        LogUtil.i("Save Person: " + person);
    }
}
