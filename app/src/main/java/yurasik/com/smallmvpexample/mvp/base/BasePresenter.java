package yurasik.com.smallmvpexample.mvp.base;

import android.os.Bundle;

import yurasik.com.smallmvpexample.util.LogUtil;

public abstract class BasePresenter<V extends BaseMvpView> {

    private V mView;

    public void attach(V mView){
        this.mView = mView;
        LogUtil.i("attach: " + mView);
    }

    public void detach(){
        LogUtil.i("detach: " + mView);
        mView = null;
    }

    public V getMvpView() {
        return mView;
    }

    public abstract void onSave(Bundle state);
    public abstract void onRestote(Bundle state);
}
