package yurasik.com.smallmvpexample.mvp.presenter;

import android.os.Bundle;
import android.text.TextUtils;

import yurasik.com.smallmvpexample.data.DataStorageInterface;
import yurasik.com.smallmvpexample.data.MockDataStorage;
import yurasik.com.smallmvpexample.mvp.base.BasePresenter;
import yurasik.com.smallmvpexample.mvp.model.Person;
import yurasik.com.smallmvpexample.mvp.view.CreatePersonMvpView;
import yurasik.com.smallmvpexample.util.LogUtil;

public class CreatePersonPresenter<V extends CreatePersonMvpView> extends BasePresenter<V>
        implements ICreatePersonPresenter<V> {

    private Person mPerson;
    private DataStorageInterface mStorage;

    @Override
    public void attach(V mView) {
        super.attach(mView);
        setDataStorage(new MockDataStorage());
    }

    @Override
    public void onSave(Bundle state) {
        LogUtil.i("onSave: " + state);
    }

    @Override
    public void onRestote(Bundle state) {
        LogUtil.i("onRestore: " + state);
    }

    @Override
    public void onClickSaveButton() {
        LogUtil.i("onClickSaveButton");
        String first = getMvpView().getFirstName();
        String last = getMvpView().getLastName();
        if (!TextUtils.isEmpty(first) && !TextUtils.isEmpty(last)) {
            mPerson = new Person();
            mPerson.setFirstName(first);
            mPerson.setLastName(last);
            savePerson(mPerson);
        }
    }

    @Override
    public void onClickClearButton() {
        mPerson = null;
        getMvpView().clearEditTexts();
    }

    @Override
    public void savePerson(Person person) {
        LogUtil.i("Person: " + person);
        mStorage.savePerson(person);
    }

    @Override
    public void setDataStorage(DataStorageInterface storage) {
        this.mStorage = storage;
    }
}
